#!/usr/bin/python

import os
from list_ops import normalize_weights
from weight_file import *
from cli import get_parser
from exercise_choice import get_exercises


EXERCISES = [
    "Repeated lines",
    "Planes with ellipses",
    "Table of ellipses",
    "Ellipse tunnel",
    "Rough perspektive",
    "Rotated boxes",
    "Organic perspektive",
]


def init(num_exercises: int):
    print(f"[+] initializing {num_exercises} weights")
    weights = [1] * num_exercises
    normalized_weights = normalize_weights(weights)
    save_weights(normalized_weights)


def get_choices(args):
    if not os.path.exists(WEIGHT_FILE):
        raise Exception("initialize the session first")

    print("[+] loading weights")

    weights = load_weights()
    if len(weights) < args.num:
        raise Exception("too many choices")

    print(f"[+] using weights: {[round(weight,2) for weight in weights]}")

    choices, new_weights = get_exercises(weights, args.num)
    choices.sort()

    print(f"[+] new weights: {[round(weight,2) for weight in new_weights]}")
    print("[+] saving new weights")
    save_weights(new_weights)

    return choices


def main():
    parser = get_parser()
    args = parser.parse_args()

    if args.init:
        init(len(EXERCISES))
        return
    if args.restore:
        restore_weights(args.restore)
        print(
            f"[+] restored weights to: {[round(weight,2) for weight in load_weights()]}"
        )
        return
    if args.restore_backup:
        restore_backup()
        return
    else:
        choices = get_choices(args)
        print(
            f"Result: Do exercises {''.join([f"{EXERCISES[choice]} ({choice+1}), " for choice in choices])[:-2]}"
        )


if __name__ == "__main__":
    main()
