from random import random
from list_ops import normalize_weights


def weighted_random_choice(weights: list[float]) -> int:
    # needs normalized weights
    r = random()
    weight_counter = 0
    for i, weight in enumerate(weights):
        weight_counter += weight
        if r < weight_counter:
            return i


def get_exercises(
    weights: list[float], num_choices, weight_multiplier=0.2
) -> tuple[list[int], list[float]]:
    weights = normalize_weights(weights)

    choices = []
    while len(choices) != num_choices:
        random_weigth_index = weighted_random_choice(weights)
        if random_weigth_index not in choices:
            # a bit inefficient I know and I don't care
            weights[random_weigth_index] *= weight_multiplier
            weights = normalize_weights(weights)
            choices.append(random_weigth_index)

    return (choices, weights)
