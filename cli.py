import argparse


def get_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="get randomized exercises based on recent choices"
    )
    parser.add_argument(
        "--restore-backup", help="restore previous backup", action="store_true"
    )
    parser.add_argument("--restore", help="restore previous N weights", type=int)
    parser.add_argument("--init", help="initialize the program", action="store_true")
    parser.add_argument("--num", type=int, help="number of random exercises", default=3)
    return parser
