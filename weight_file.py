from shutil import copyfile
import os
import json


WORKINGDIR = os.path.dirname(__file__)
WEIGHT_FILE = os.path.join(WORKINGDIR, "weights")
WEIGHT_FILE_BKP = os.path.join(WORKINGDIR, "weights-bkp")


def backup_weights():
    if os.path.exists(WEIGHT_FILE):
        copyfile(os.path.join(WEIGHT_FILE), WEIGHT_FILE_BKP)


def save_weights(weights):
    backup_weights()
    with open(WEIGHT_FILE, "a") as f:
        f.write(str(weights) + "\n")


def load_weights() -> list[float]:
    with open(WEIGHT_FILE) as f:
        last_line = f.readlines()[-1]
        weights = json.loads(last_line)
    return weights


def restore_weights(iterations=1):
    backup_weights()
    print("[+] restoring previous weights")
    with open(WEIGHT_FILE, "r") as f:
        lines = f.readlines()[:-iterations]
    with open(WEIGHT_FILE, "w") as f:
        f.writelines(lines)
    print(f"[+] last {iterations} iterations removed")


def restore_backup():
    print("[+] restoring backup")
    if not os.path.exists(WEIGHT_FILE_BKP):
        raise Exception("no backup found")
    copyfile(WEIGHT_FILE_BKP, WEIGHT_FILE)
    print("[+] backup restored")
