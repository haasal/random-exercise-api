def flatten(xss):
    return [x for xs in xss for x in xs]


def intersect(a, b):
    intersection = [value for value in a if value in b]
    return intersection


def normalize_weights(lst: list[float]) -> list[float]:
    lst_sum = sum(lst)
    return [item / lst_sum for item in lst]
